![DASF Logo](https://git.geomar.de/digital-earth/dasf/dasf-messaging-python/-/raw/master/docs/_static/dasf_logo.svg)

[![DOI](https://git.geomar.de/digital-earth/dasf/dasf-web/-/raw/master/doi_badge.svg)](https://doi.org/10.5880/GFZ.1.4.2021.006)
[![npm version](https://badge.fury.io/js/dasf-web.svg)](https://badge.fury.io/js/dasf-web)
[![status](https://joss.theoj.org/papers/e8022c832c1bb6e879b89508a83fa75e/status.svg)](https://joss.theoj.org/papers/e8022c832c1bb6e879b89508a83fa75e)

## DASF: Web

`dasf-web` is part of the Data Analytics Software Framework (DASF, https://git.geomar.de/digital-earth/dasf), 
developed at the GFZ German Research Centre for Geosciences (https://www.gfz-potsdam.de). 
It is funded by the Initiative and Networking Fund of the Helmholtz Association through the Digital Earth project 
(https://www.digitalearth-hgf.de/).

`dasf-web` collects all web components for the data analytics software framework DASF. It provides ready to use interactive data visualization components like time series charts, radar plots, stacked-parameter-relation (spr) and more, as well as a powerful map component for the visualization of spatio-temporal data. Moreover dasf-web includes the web bindings for the DASF RPC messaging protocol and therefore allows to connect any algorithm or method (e.g. via the `dasf-messaging-python` implementation) to the included data visualization components. Because of the component based architecture the integrated method could be deployed anywhere (e.g. close to the data it is processing), while the interactive data visualizations are executed on the local machine. dasf-web is implemented in Typescript and uses Vuejs/Vuetify, Openlayers and D3 as a technical basis.


### Service Desk

For everyone without a Geomar Gitlab account, we setup the Service Desk feature for this repository.
It lets you communicate with the developers via a repository specific eMail address. Each request will be tracked via the Gitlab issuse tracker.

eMail: [gitlab+digital-earth-dasf-dasf-web-1982-issue-@git-issues.geomar.de](mailto:gitlab+digital-earth-dasf-dasf-web-1982-issue-@git-issues.geomar.de)


### NPM Package `dasf-web`
[![npm version](https://badge.fury.io/js/dasf-web.svg)](https://badge.fury.io/js/dasf-web)

`DASF: Web` is released as a NPM package called `dasf-web`

You may install it (and save it to your `package.json`) via:

```bash
npm install dasf-web --save
```

Note: You will also need to install the peer dependencies listed below. Also consider to have a detailled look at the usage examples below.

### Usage

https://git.geomar.de/digital-earth/dasf/dasf-full-example

or

https://git.geomar.de/digital-earth/dasf/dasf-app-template


### Peer dependencies

The library expects the following peer dependencies:
```json
    "openlayers"
    "proj4"
    "vuetify"
    "vue-class-component"
    "vue-template-compiler"
```

### Documentation

see https://digital-earth.pages.geomar.de/dasf/dasf-messaging-python/

### Recommended Software Citation

`Eggert, Daniel; Rabe, Daniela; Dransch, Doris (2021): DASF: Web: Web components for the data analytics software framework. GFZ Data Services. https://doi.org/10.5880/GFZ.1.4.2021.006`

### License
```
Copyright 2021 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences, Potsdam, Germany / DASF Data Analytics Software Framework

Licensed under the Apache License, Version 2.0 (the "License");
you may not use these files except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

### Contact
Dr.-Ing. Daniel Eggert  
eMail: <daniel.eggert@gfz-potsdam.de>


Helmholtz Centre Potsdam GFZ German Research Centre for Geoscienes  
Section 1.4 Remote Sensing & Geoinformatics  
Telegrafenberg  
14473 Potsdam  
Germany
