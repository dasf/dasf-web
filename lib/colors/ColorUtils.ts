export default class ColorUtils {

    public static setAlpha<T extends string | number[]>(color: T, alpha: number): T {
        const _alpha = Math.round(Math.min(Math.max(alpha || 1, 0), 1) * 255);

        if (typeof color == 'string') {
            if (color.startsWith('#')) {
                // its a hex string - check whether we already have an alpha value
                const alphaHex = _alpha.toString(16).toUpperCase();

                return (color.slice(0, 7) + alphaHex) as T;
            } else if (color.startsWith('rgb')) {
                console.warn('not yet implemented!');
                return color as T;
            }
        } else if (Array.isArray(color)) {
            let c = color.slice(0, 3) as number[];
            c.push(_alpha);
            return c as T;
        }
    }

    public static hex2Rgb(hex: string): number[] {
        return hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i
            , (m, r, g, b) => '#' + r + r + g + g + b + b)
            .substring(1).match(/.{2}/g)
            .map(x => parseInt(x, 16));
    }

}

