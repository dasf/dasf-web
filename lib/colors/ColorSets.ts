export enum DataNature {
    Sequential,
    Diverging,
    Qualitative
}

export enum ColorFlavor {
    Regular,
    Accent,
    Dark,
    Paired,
    Pastel
}

export default class ColorSets {

    private static readonly QUALITATIVE_REGULAR = [
        '#8dd3c7', '#ffffb3', '#bebada', '#fb8072', '#80b1d3', '#fdb462', '#b3de69', '#fccde5', '#d9d9d9', '#bc80bd', '#ccebc5', '#ffed6f'];

    public static getColors(numColors: number, flavor: ColorFlavor, nature: DataNature): string[] {
        switch (nature) {
            case DataNature.Qualitative:
                return ColorSets.getQualitative(numColors, flavor);
            default:
                console.warn('unsupported DataNature ' + nature + ' | using qualitative fallback');
                return ColorSets.getQualitative(numColors, flavor);
        }
    }

    public static getQualitative(numColors: number, flavor: ColorFlavor): string[] {
        let colors: string[];

        switch (flavor) {
            case ColorFlavor.Regular:
                colors = this.QUALITATIVE_REGULAR;
                break;
            default:
                console.warn('unsupported ColorFlavor ' + flavor + ' | using regular fallback');
                colors = this.QUALITATIVE_REGULAR;
                break;
        }

        if (numColors == 0) {
            throw Error('Invalid number of colors (requested: ' + numColors + ' | min allowed: 1');
        }

        if (numColors > colors.length) {
            throw Error('Invalid number of colors (requested: ' + numColors + ' | max allowed: ' + this.QUALITATIVE_REGULAR.length);
        }

        return colors.slice(0, numColors);
    }

}