<template>
  <div ref="histogramchart" />
</template>

<style>
.axis {
  font-size: 1em !important;
}
</style>

<script lang="ts">
import { Component, Vue, Prop, Emit, Ref, Watch } from 'vue-property-decorator';
import * as d3 from 'd3';
import Histogram from '../model/Histogram';
import ColorSets, { ColorFlavor, DataNature } from '../colors/ColorSets';
import MathUtils from '../util/MathUtils';
import Filter from '../map/model/Filter';

@Component({})
export default class HistogramChart extends Vue {
  private static revCount = 0;

  private static readonly TITLE_FONT_SIZE_EM = 1.2;
  private static readonly SUBTITLE_FONT_SIZE_EM = 1;

  @Prop({ type: String, required: true }) chartTitle!: string;
  @Prop({ type: Histogram, required: true }) data!: Histogram;
  @Prop({ type: Number, required: false, default: Number.NaN }) xMinFilter!: number;
  @Prop({ type: Number, required: false, default: Number.NaN }) xMaxFilter!: number;
  @Prop({ type: Number, required: false }) xDomainStart!: number;
  @Prop({ type: Number, required: false }) xDomainEnd!: number;
  @Prop({ type: Number, required: false }) yDomainStart!: number;
  @Prop({ type: Number, required: false }) yDomainEnd!: number;
  @Prop({ type: String, required: false, default: '' }) chartSubtitle!: string;
  @Prop({ type: String, required: false, default: 'steelblue' }) color!: string;
  @Prop({ type: Number, required: false, default: 500 }) chartWidth!: number;
  @Prop({ type: Number, required: false, default: 500 }) chartHeight!: number;
  @Prop({ type: Boolean, required: false, default: false }) showGrid!: boolean;

  private colors!: Map<string, string>;

  @Ref('histogramchart') readonly targetElement!: HTMLElement;

  private createChartId(): string {
    return 'histogramchart-' + ++HistogramChart.revCount;
  }

  @Watch('data')
  @Watch('xMinFilter')
  @Watch('xMaxFilter')
  protected onDataChanged(): void {
    this.build();
  }

  protected mounted(): void {
    if (!this.targetElement.id) {
      this.targetElement.id = this.createChartId();
    }

    if (this.data && this.data.values.length > 0) {
      this.build();
    }
  }

  private build(): void {
    if (!this.targetElement || !this.targetElement.id) {
      throw new Error('invalid chart container or missing id');
    }

    // clear exisiting data
    d3.select('#' + this.targetElement.id)
      .selectAll('*')
      .remove();

    const margin = { left: 25, top: 60, bottom: 20, right: 15 };
    const width = this.chartWidth - margin.left - margin.right;
    const height = this.chartHeight - margin.top - margin.bottom;

    let svg = d3
      .select('#' + this.targetElement.id)
      .classed('svg-container', true)
      .append('svg')
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr(
        'viewBox',
        -margin.left +
          ' 0 ' +
          (this.chartWidth + margin.left + margin.right) +
          ' ' +
          (this.chartHeight + margin.bottom + margin.top)
      )
      .classed('svg-content-responsive', true);

    // render chart title
    let chartTitleY = 12 * HistogramChart.TITLE_FONT_SIZE_EM + 1;
    if (this.chartTitle && this.chartTitle.length > 0) {
      svg
        .append('text')
        .attr('x', this.chartWidth / 2)
        .attr('y', chartTitleY)
        .attr('text-anchor', 'middle')
        .attr('fill', 'currentColor')
        .style('font-size', HistogramChart.TITLE_FONT_SIZE_EM + 'em')
        .text(this.chartTitle);
    }

    // render chart subtitle
    let subtitleY = chartTitleY + 12 * HistogramChart.SUBTITLE_FONT_SIZE_EM + 6;
    if (this.chartTitle && this.chartTitle.length > 0) {
      svg
        .append('text')
        .attr('x', this.chartWidth / 2)
        .attr('y', subtitleY)
        .attr('text-anchor', 'middle')
        .attr('fill', '#aaa')
        .style('font-size', HistogramChart.SUBTITLE_FONT_SIZE_EM + 'em')
        .text(this.chartSubtitle);
    }

    const chart = svg
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    let xDomain: number[] = [this.data.min, this.data.max];
    if (this.xDomainStart != undefined) {
      xDomain[0] = this.xDomainStart;
    }
    if (this.xDomainEnd != undefined) {
      xDomain[1] = this.xDomainEnd;
    }
    // X axis: scale and draw:
    const x = d3
      .scaleLinear()
      .domain(xDomain)
      .range([0, width]);

    chart
      .append('g')
      .attr('class', 'axis')
      .attr('transform', 'translate(0, ' + height + ')')
      .call(d3.axisBottom(x));

    //yScale
    let yDomain: number[] = [0, Math.max(...this.data.values)];
    if (this.yDomainStart != undefined) {
      yDomain[0] = this.yDomainStart;
    }
    if (this.yDomainEnd != undefined) {
      yDomain[1] = this.yDomainEnd;
    }

    var y = d3
      .scaleLinear()
      .domain(yDomain)
      .nice()
      .range([height, 0]);

    // svg.append("g")
    //     .call(d3.axisLeft(y));

    var yAxis = d3.axisLeft().scale(y);

    chart
      .append('g')
      .attr('class', 'x axis')
      .call(yAxis)
      .selectAll('text');

    //grid lines
    if (this.showGrid) {
      chart
        .append('g')
        .attr('class', 'grid')
        .call(
          d3
            .axisLeft()
            .scale(y)
            .tickSize(-width, 0, 0)
            .tickFormat('')
        );
    }

    // append the bar rectangles to the svg element
    const data: Histogram = this.data;
    let values: number[] = this.getFilteredHistogramValues();

    chart
      .selectAll('rect')
      .data(values)
      .enter()
      .append('rect')
      .attr('x', 1)
      .attr('transform', function(d, i) {
        return 'translate(' + x(data.getX(i)) + ',' + y(d) + ')';
      })
      .attr('width', function(d, i) {
        return 0.9 * (x(data.getX(i + 1)) - x(data.getX(i)));
      })
      .attr('height', function(d) {
        return height - y(d);
      })
      .style('fill', 'steelblue');
  }

  protected getFilteredHistogramValues(): number[] {
    let values: number[] = [];

    for(let i=0; i<this.data.values.length; ++i) {
      let x = this.data.getX(i);
      if((!Number.isNaN(this.xMinFilter) && x<this.xMinFilter) || (!Number.isNaN(this.xMaxFilter) && x>this.xMaxFilter)) {
       values.push(0); 
      } else {
        values.push(this.data.values[i]);
      }
    }

    return values;
  }
}
</script>
