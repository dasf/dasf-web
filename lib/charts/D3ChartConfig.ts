export enum YAxis {
  left, right
}

export enum RenderStyle {
  line, bar, area, scatter
}
