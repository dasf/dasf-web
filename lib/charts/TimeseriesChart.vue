<template>
  <div ref="timeserieschart" />
</template>


<script lang="ts">
import * as d3 from 'd3';
import { Component, Prop, Ref, Vue } from 'vue-property-decorator';
import { DateValuePair, Timeseries, Header } from '../model/Timeseries';
import { RenderStyle, YAxis } from './D3ChartConfig';

const VERSION: number[] = [0, 0, 2];
const NAME = 'TimeseriesChart';

export class TimeseriesConfig {
  constructor(
    public timeseries: Timeseries[],
    public style: RenderStyle,
    public color: string,
    public label: string,
    public side: YAxis,
    public onClick?: (
      ts: Timeseries,
      chartElement: any,
      update?: (ts: Timeseries) => void
    ) => void
  ) {}
}

class AxisConfig {
  constructor(
    public side: YAxis,
    public label: string = '',
    public domainRange: [number, number] = [Number.NaN, Number.NaN]
  ) {}

  public reset(): void {
    this.label = '';
    this.setDomainRange(Number.NaN, Number.NaN);
  }

  public include(timeseries: Timeseries[]): void {
    for (let ts of timeseries) {
      if (this.label == '') {
        // set label from timeseries
        this.label = ts.header.name + ' (' + ts.header.unit + ')';
      }

      // determine timeseries domain bounds
      let tsBounds = ts.getValueBounds();
      if (this.hasDomainBounds()) {
        // update bounds
        this.domainRange[0] = Math.min(tsBounds[0], this.domainRange[0]);
        this.domainRange[1] = Math.max(tsBounds[1], this.domainRange[1]);
      } else {
        // initialize bounds
        this.domainRange = tsBounds;
      }
    }
  }

  public hasDomainBounds(): boolean {
    return (
      !Number.isNaN(this.domainRange[0]) && !Number.isNaN(this.domainRange[0])
    );
  }

  public setDomainRange(lower: number, upper: number): void {
    this.domainRange[0] = lower;
    this.domainRange[1] = upper;
  }
}

export interface ChartHandler {
  onD3Container(d3Container: HTMLElement, d3: object): void;
  onBarPlot(barPlot: object, d3: object): void;
  onMinimap(
    minimapPlot: object,
    minimapTopOffset: number,
    xMinimapScale: object,
    yMinimapScale: object,
    d3: object
  ): void;
  onBuilt(d3: object): void;
  onMinimapRange(
    source: TimeseriesChart,
    lower: number,
    upper: number,
    d3: object
  ): void;
}

@Component({})
export default class TimeseriesChart extends Vue implements ChartHandler {
  @Prop({ type: String, required: false, default: '' }) chartTitle!: string;
  @Prop({
    type: Array,
    required: false,
    default: () => {
      [];
    },
  })
  tsConfig!: TimeseriesConfig[];
  @Prop({
    type: Array,
    required: false,
    default: () => {
      [];
    },
  })
  yDomainLeft!: number[];
  @Prop({
    type: Array,
    required: false,
    default: () => {
      [];
    },
  })
  yDomainRight!: number[];
  @Prop({ type: Number, required: false, default: 1.0 }) fontScale!: number;
  // @Prop({ type: Date, required: false, default: null }) markDate!: Date;
  // @Prop({ type: String, required: false, default: '' }) markDateLabel!: string;

  private static revCount = 0;

  private handlers: ChartHandler[] = [];

  private margin = {
    top: 40 * this.fontScale,
    right: 50 * this.fontScale,
    bottom: 40 * this.fontScale,
    left: 50 * this.fontScale,
  };
  private width = 1400;
  private height = 550;
  private heightMinimap = 50;
  private timeseries: TimeseriesConfig[] = [];
  private title: string = '';
  private svg;
  private plotCanvas;
  private xScale;
  private xMinimapScale;
  private xAxis;
  private yScales: any[] = [];
  protected barPlot;
  private minimapData: TimeseriesConfig;
  private minimapTop;
  private plotMinimap;
  private tooltip;
  private startDate: Date = new Date();
  private endDate: Date = new Date();
  private lineList: { className: string; update: Function }[] = [];
  private idleTimeout;
  private minimap;
  private minTimeDeltaMillis;
  private minimapOffsetLeft = 0.2;
  private minimapOffsetRight = 0.8;
  private minimapBrush;
  private updatingBrush = false;
  private plotFactor: number = 0.8;

  private left: AxisConfig = new AxisConfig(YAxis.left);
  private right: AxisConfig = new AxisConfig(YAxis.right);

  private readonly AREA_FILL_OPACITY = 0.35;

  @Ref('timeserieschart') readonly targetElement!: HTMLElement;

  protected mounted(): void {
    if (!this.targetElement.id) {
      this.targetElement.id = this.createChartId();
    }

    if (this.chartTitle && this.chartTitle.length > 0) {
      this.setTitle(this.chartTitle);
    }

    if (this.tsConfig && this.tsConfig.length > 0) {
      // we have initial ts configs - register them and build chart
      for (let i = 0; i < this.tsConfig.length; ++i) {
        let tsc: TimeseriesConfig = this.tsConfig[i];
        this.addTimeseries(
          tsc.side,
          tsc.style,
          tsc.color,
          tsc.label,
          tsc.timeseries,
          tsc.onClick,
          false,
          false
        );
      }

      if (this.yDomainLeft && this.yDomainLeft.length > 1) {
        this.setYDataRange(
          YAxis.left,
          this.yDomainLeft[0],
          this.yDomainLeft[1],
          false
        );
      }

      if (this.yDomainRight && this.yDomainRight.length > 1) {
        this.setYDataRange(
          YAxis.right,
          this.yDomainRight[0],
          this.yDomainRight[1],
          false
        );
      }

      this.useDataDateBounds();
    }
  }

  private createChartId(): string {
    return 'timeserieschart-' + ++TimeseriesChart.revCount;
  }

  public addHandler(handler: ChartHandler): void {
    if (this.handlers == undefined) {
      this.handlers = [];
    }

    this.handlers.push(handler);
  }

  public clearHandlers(): void {
    if (this.handlers) {
      this.handlers.length = 0;
    }
  }

  public onD3Container(d3Container: HTMLElement): void {
    for (const handler of this.handlers) {
      handler.onD3Container(d3Container, d3);
    }
  }

  public onBarPlot(barPlot: object): void {
    for (const handler of this.handlers) {
      handler.onBarPlot(barPlot, d3);
    }
  }

  public onMinimap(
    minimapPlot: object,
    minimapTopOffset: number,
    xMinimapScale: object,
    yMinimapScale: object
  ): void {
    for (const handler of this.handlers) {
      handler.onMinimap(
        minimapPlot,
        minimapTopOffset,
        xMinimapScale,
        yMinimapScale,
        d3
      );
    }
  }

  public onBuilt(): void {
    for (const handler of this.handlers) {
      handler.onBuilt(d3);
    }
  }

  public onMinimapRange(
    source: TimeseriesChart,
    lower: number,
    upper: number
  ): void {
    for (const handler of this.handlers) {
      handler.onMinimapRange(source, lower, upper, d3);
    }
  }

  public setTitle(title: string): void {
    this.title = title;
  }

  /**
   * Offset of the selected x-axis area.<br>
   * default: 0.2 .. 0.8 <br>
   * for entire date range use: 0 .. 1
   * @param left
   * @param right
   */
  public setMinimapOffsets(
    left = 0.2,
    right = 0.8,
    updateBrush = false,
    updateChart = false,
    notifyHandler = false
  ): void {
    this.minimapOffsetLeft = left;
    this.minimapOffsetRight = right;

    if (updateBrush && this.minimapBrush) {
      this.updatingBrush = true;
      this.minimap
        .select('.brush')
        .call(this.minimapBrush.move, [
          this.width * this.minimapOffsetLeft,
          this.width * this.minimapOffsetRight,
        ]);
      this.updatingBrush = false;
    }

    if (updateChart) {
      this.updateChart(this);
    }
    if (notifyHandler) {
      this.onMinimapRange(
        this,
        this.minimapOffsetLeft,
        this.minimapOffsetRight
      );
    }
  }

  public getConfig(side: YAxis): AxisConfig {
    return side == YAxis.left ? this.left : this.right;
  }

  public resetConfigs(): void {
    this.left.reset();
    this.right.reset();
  }

  public clearTimeseries(): void {
    this.timeseries.length = 0;
    this.minimapData = undefined;
    this.lineList.length = 0;
    // clear the axis config as well
    this.resetConfigs();
  }

  public addTimeseries(
    side: YAxis,
    style: RenderStyle,
    color: string,
    label: string,
    timeseries: Timeseries[] | Timeseries,
    onClick?: (
      ts: Timeseries,
      chartElement: any,
      update?: (ts: Timeseries) => void
    ) => void,
    atFront = false,
    rebuild = true,
    showOnMinimap = false
  ): number {
    let ts: Timeseries[] = Array.isArray(timeseries)
      ? timeseries
      : [timeseries];
    let tsConf = new TimeseriesConfig(ts, style, color, label, side, onClick);
    let tsId = Number.NaN;
    if (atFront) {
      tsId = 0;
      this.timeseries.unshift(tsConf);
    } else {
      tsId = this.timeseries.length;
      this.timeseries.push(tsConf);
    }

    this.getConfig(side).include(ts);

    // update minimap data
    if (showOnMinimap) {
      this.minimapData = tsConf;
    }

    if (rebuild) {
      this.build();
    }

    return tsId;
  }

  public removeTimeseries(tsId: number, rebuild = true): void {
    delete this.timeseries[tsId];
  }

  public setPlotFactor(plotFactor: number): void {
    this.plotFactor = plotFactor;
  }

  public setYDataRange(
    side: YAxis,
    lower: number,
    upper: number,
    updateChart = false
  ): void {
    this.getConfig(side).setDomainRange(lower, upper);

    if (updateChart) {
      this.build();
    }
  }

  private build(): void {
    if (!Array.isArray(this.timeseries) || !this.timeseries.length) {
      // nothing to build
      return;
    }

    // init the chart
    this.setupChart();

    // init the axis
    this.addXAxis();
    this.addYAxis();

    // render the timeseries
    // console.log('render timeseries', this.timeseries);
    for (let tsc of this.timeseries) {
      if (tsc == undefined) {
        continue;
      }

      let side = tsc.side;
      let timeseries = tsc.timeseries;
      let style = tsc.style;
      let color = tsc.color;
      let onClick = tsc.onClick;

      const parent = this.plotCanvas.append('g').attr('group', tsc.label);

      for (let ts of timeseries) {
        if (ts) {
          // render timeseries data
          switch (style) {
            case RenderStyle.bar:
              this.renderBarTimeseries(ts, side, color, parent, onClick);
              this.minTimeDeltaMillis = this.getSmallestTemporalDelta(
                this.sortAndFilterData(ts.data)
              );
              break;
            case RenderStyle.line:
              this.renderLineTimeseries(ts, side, color, parent, onClick);
              break;
            case RenderStyle.area:
              this.renderAreaTimeseries(ts, side, color, parent, onClick);
              break;
            case RenderStyle.scatter:
              this.renderScatterTimeseries(ts, side, color, parent, onClick);
              break;
            default:
              throw new Error('Unsupported style');
          }
        }
      }
    }

    // move path to front
    d3.select('path').each(function (this: any) {
      this.parentNode.appendChild(this);
    });

    // render the minimap (ignoring the style at the moment)
    this.addMinimap();

    // render the Legend
    this.addLegend();

    this.onBuilt();
  }

  private clear(): void {
    if (this.svg) {
      this.svg.selectAll('*').remove();
    }

    this.lineList = [];
  }

  protected setupChart(): void {
    // clear the chart
    this.clear();

    let viewBoxWidth =
      this.width + this.margin.left * 2 + this.margin.right * 2;
    let viewBoxHeight =
      this.height +
      this.margin.top +
      this.margin.bottom +
      this.heightMinimap +
      400;

    if (!this.targetElement || !this.targetElement.id) {
      // throw new Error('invalid chart container or missing id');
      return;
    }

    this.onD3Container(this.targetElement);

    // init the svg canvas
    if (!this.svg) {
      this.svg = d3
        .select('#' + this.targetElement.id)
        .classed('svg-container', true)
        .append('svg')
        .attr('preserveAspectRatio', 'xMinYMin meet')
        .attr(
          'viewBox',
          -this.margin.left + ' 0 ' + viewBoxWidth + ' ' + viewBoxHeight
        )
        .classed('svg-content-responsive', true)
        .append('g')
        .attr(
          'transform',
          'translate(' + this.margin.left + ',' + this.margin.top + ')'
        );
    }

    // render chart title
    if (this.title && this.title.length > 0) {
      this.svg
        .append('text')
        .attr('x', this.width / 2)
        .attr('y', 0 - this.margin.top / 3)
        .attr('text-anchor', 'middle')
        .attr('fill', 'currentColor')
        .style('font-size', 1.5 * this.fontScale + 'em')
        .text(this.title);
    }

    // define clipping area
    this.svg
      .append('defs')
      .append('svg:clipPath')
      .attr('id', 'clip')
      .append('svg:rect')
      .attr('width', this.width - 1)
      .attr('height', this.height + 3)
      .attr('x', 1)
      .attr('y', -2);

    // init chart plot canvas based on defined clipping area
    this.plotCanvas = this.svg.append('g').attr('clip-path', 'url(#clip)');

    // init tooltip element
    if (this.tooltip == null) {
      this.tooltip = d3.select('body').append('div').attr('class', 'toolTip');
    }
  }

  protected addXAxis(): void {
    // define x axis time scale
    this.xScale = d3
      .scaleUtc()
      .domain([this.startDate, this.endDate])
      .rangeRound([0, this.width]);

    // render time x axis
    this.xAxis = this.svg
      .append('g')
      .attr('class', 'context axis')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(d3.axisBottom(this.xScale).scale(this.xScale));

    if (this.fontScale != 1.0) {
      this.xAxis.selectAll('text').attr('font-size', this.fontScale + 'em');
    }
  }

  protected addYAxis(): void {
    for (let side of [YAxis.left, YAxis.right]) {
      let axisConfig = this.getConfig(side);
      if (!axisConfig.hasDomainBounds()) {
        // missing domain bounds - don't render
        continue;
      }

      // render y axis label
      this.svg
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', this.getYAxisLabelPos(side))
        .attr('x', 0 - this.height / 2)
        .style('text-anchor', 'middle')
        .attr('fill', 'currentColor')
        .attr('font-size', this.fontScale + 'em')
        .text(axisConfig.label);

      // define y axis scale

      this.yScales[side] = d3
        .scaleLinear()
        .domain(axisConfig.domainRange)
        .nice()
        .range([this.height, 0]);

      // append yaxis element
      let axis = this.svg.append('g').attr('class', 'axis');

      // render axsis
      let rotatedLabelOffset = '';
      switch (side) {
        case YAxis.left:
          axis.call(d3.axisLeft(this.yScales[side]));

          rotatedLabelOffset = '-0.8em';
          break;
        case YAxis.right:
          axis
            .attr('transform', 'translate(' + this.width + ',0)')
            .call(d3.axisRight(this.yScales[side]));

          rotatedLabelOffset = '1.3em';
          break;
      }

      // rotate label if numbers too large
      if (axisConfig.domainRange[1] > 1000) {
        axis
          .selectAll('text')
          .attr('transform', 'rotate(-90)')
          .attr('dy', rotatedLabelOffset)
          .style('text-anchor', 'middle')
          .attr('x', 0);
      }

      if (this.fontScale != 1.0) {
        axis.selectAll('text').attr('font-size', this.fontScale + 'em');
      }
    }
  }

  private timeMarkLine: SVGLineElement = null;
  private timeMarkLabel: SVGTextElement = null;

  public markTime(
    date: Date,
    label: string = '',
    color: string = 'magenta'
  ): void {
    // remove 'old' markers
    if (this.timeMarkLine !== null) {
      this.timeMarkLine.remove();
      this.timeMarkLine = null;
    }

    if (this.timeMarkLabel !== null) {
      this.timeMarkLabel.remove();
      this.timeMarkLabel = null;
    }

    if (date) {
      this.timeMarkLine = this.plotCanvas
        .append('line')
        .attr('stroke', color)
        .attr('x1', this.xScale(date))
        .attr('y1', 0)
        .attr('x2', this.xScale(date))
        .attr('y2', this.height)
        .attr('width', 2)
        .attr('id', 'mark-date');
    }

    if (label && label.length > 0) {
      const filter = this.plotCanvas
        .append('defs')
        .append('filter')
        .attr('x', '0')
        .attr('y', '0')
        .attr('width', '1')
        .attr('height', '1')
        .attr('id', 'mark-date-solid');

      filter.append('feFlood').attr('flood-color', color);

      filter
        .append('feComposite')
        .attr('in', 'SourceGraphic')
        .attr('operator', 'xor');

      this.timeMarkLabel = this.plotCanvas
        .append('text')

        .attr('x', this.xScale(date))
        .attr('y', 20 * this.fontScale)
        .attr('font-size', this.fontScale * 1.5 + 'em')
        .attr('filter', 'url(#mark-date-solid)')
        .text(label);
    }
  }

  protected renderBarTimeseries(
    ts: Timeseries,
    side: YAxis,
    color: string,
    parent: any,
    onClick: (
      ts: Timeseries,
      chartElement: any,
      update?: (ts: Timeseries) => void
    ) => void
  ): void {
    // get ts data and header
    let tsData: DateValuePair[] = this.sortAndFilterData(ts.data);
    let tsHeader = ts.header;

    var countBars = 0;

    let scale = this.yScales[side];

    this.barPlot = parent
      .append('g')
      .attr('fill', color)
      .selectAll('rect') //rect
      .enter()
      .data(tsData)
      .join('rect') //rect
      .attr('x', (element: DateValuePair) => this.xScale(element.date))
      .attr('y', (element: DateValuePair) => scale(element.field))
      .attr('id', function (d) {
        countBars++;
        return 'barId_' + countBars;
      })
      .attr('width', 2)
      .attr(
        'height',
        (element: DateValuePair) => scale(0) - scale(element.field)
      )
      .on('mousemove', (element: DateValuePair) => {
        this.tooltip
          .style('left', d3.event.pageX - 50 + 'px')
          .style('top', d3.event.pageY - 70 + 'px')
          .style('display', 'inline-block')
          .html(
            element.date.toISOString().substring(0, 10) +
              '<br>' +
              tsHeader.name +
              ': ' +
              element.field +
              ' ' +
              tsHeader.unit
          );
      })
      .on('mouseout', () => {
        this.tooltip.style('display', 'none');
      });

    this.onBarPlot(this.barPlot);

    if (onClick) {
      this.barPlot.attr('cursor', 'pointer').on('click', (item) => {
        onClick(ts, d3.event.target); // TODO: also pass clicked bar
      });
    }
    // TODO: add on click listener to bar plots

    // .on('click', event => {
    //   this.selectedEvents.addEvent('#' + d3.event.target.id, event.date);
    //   this.showSelectedEvents();
    // })
    // .on('contextmenu', event => {
    //   this.selectedEvents.removeEvent('#' + d3.event.target.id);
    //   this.showSelectedEvents();
    // });
  }

  public useDataDateBounds(): void {
    let dateBounds: [Date, Date] = null;

    for (let tsc of this.timeseries) {
      if (tsc == undefined) {
        continue;
      }

      for (let ts of tsc.timeseries) {
        let tsDateBounds = ts.getDateBounds();

        if (dateBounds) {
          // update
          if (dateBounds[0] > tsDateBounds[0]) {
            dateBounds[0] = tsDateBounds[0];
          }
          if (dateBounds[1] < tsDateBounds[1]) {
            dateBounds[1] = tsDateBounds[1];
          }
        } else {
          // init
          dateBounds = tsDateBounds;
        }
      }
    }

    this.dateRangeUpdate(dateBounds[0], dateBounds[1]);
  }

  private getYAxisLabelPos(side: YAxis): number {
    switch (side) {
      case YAxis.left:
        return -this.margin.left;
      case YAxis.right:
        return this.width + this.margin.right + 13 * this.fontScale;
    }

    throw new Error('unsupported yaxis side');
  }

  protected renderAreaTimeseries(
    ts: Timeseries,
    side: YAxis,
    color: string,
    parent: any,
    onClick: (
      ts: Timeseries,
      chartElement: any,
      update?: (ts: Timeseries) => void
    ) => void
  ): void {
    let tsData: DateValuePair[] = this.sortAndFilterData(ts.data);

    // console.log('render area plot ', color);
    let scale = this.yScales[side];

    // init line plot
    let area = d3
      .area()
      .x((element: DateValuePair) => this.xScale(element.date)) // set the x values for the line generator
      .y0(scale(0))
      .y1((element: DateValuePair) => scale(element.field)) // set the y values for the line generator
      .curve(d3.curveMonotoneX); // apply smoothing to the line

    // render line plot to plot canvas
    let lineId = this.lineList.length;
    let areaPath = parent.append('path');
    areaPath
      .datum(tsData) // 10. Binds data to the line
      .attr('class', 'line_' + lineId) // Assign a class for styling
      .attr('d', area)
      .attr('fill', color)
      .attr('fill-opacity', this.AREA_FILL_OPACITY);

    if (onClick) {
      areaPath.attr('cursor', 'pointer').on('click', (item) => {
        onClick(ts, areaPath, (newTs: Timeseries) => {
          // update
          let tsData: DateValuePair[] = this.sortAndFilterData(newTs.data);
          areaPath.datum(tsData);
          this.updateChart(this);
        });
      });
    }

    this.lineList.push({ className: '.line_' + lineId, update: area });
  }

  protected renderLineTimeseries(
    ts: Timeseries,
    side: YAxis,
    color: string,
    parent: any,
    onClick: (
      ts: Timeseries,
      chartElement: any,
      update?: (ts: Timeseries) => void
    ) => void
  ): void {
    let tsData: DateValuePair[] = this.sortAndFilterData(ts.data);
    let scale = this.yScales[side];

    // init line plot
    let newLine = d3
      .line()
      .x((element: DateValuePair) => this.xScale(element.date)) // set the x values for the line generator
      .y((element: DateValuePair) => scale(element.field)) // set the y values for the line generator
      .curve(d3.curveMonotoneX); // apply smoothing to the line

    // render line plot to plot canvas
    let lineId = this.lineList.length;
    let linePath = parent.append('path');

    linePath
      .datum(tsData) // 10. Binds data to the line
      .attr('class', 'line_' + lineId) // Assign a class for styling
      .attr('d', newLine)
      .attr('stroke', color)
      .attr('stroke-width', 2)
      .attr('fill', 'none');

    if (onClick) {
      linePath.attr('cursor', 'pointer').on('click', (item) => {
        onClick(ts, d3.event.target);
      });
    }

    this.lineList.push({ className: '.line_' + lineId, update: newLine });
  }

  protected renderScatterTimeseries(
    ts: Timeseries,
    side: YAxis,
    color: string,
    parent: any,
    onClick: (
      ts: Timeseries,
      chartElement: any,
      update?: (ts: Timeseries) => void
    ) => void
  ): void {
    let tsData: DateValuePair[] = this.sortAndFilterData(ts.data);
    let scale = this.yScales[side];

    // Add dots
    parent.append('g')
    .selectAll("dot")
    .data(tsData)
    .enter()
    .append("circle")
      .attr("cx", (element: DateValuePair) => { return this.xScale(element.date) } )
      .attr("cy", (element: DateValuePair) => { return scale(element.field) } )
      .attr("r", 2)
      .style("fill", color)


    // if (onClick) {
    //   linePath.attr('cursor', 'pointer').on('click', (item) => {
    //     onClick(ts, d3.event.target);
    //   });
    // }

    // this.lineList.push({ className: '.line_' + lineId, update: newLine });
  }

  protected addMinimap(): void {
    if (!this.minimapData || !this.minimapData.timeseries) {
      return;
    }

    let data = this.sortAndFilterData(this.minimapData.timeseries[0].data);

    var margin2 = { top: 20, right: 20, bottom: 0, left: 40 };
    this.minimapTop = this.height + margin2.top - margin2.bottom + 50;
    this.xMinimapScale = d3
      .scaleUtc()
      .domain([this.startDate, this.endDate])
      .range([0, this.width]);
    var yMinimapScale = d3
      .scaleLinear()
      .range([this.heightMinimap, 0])
      .domain([0, d3.max(data, (v: DateValuePair) => v.field)]);

    this.minimap = this.svg
      .append('g')
      .attr('class', 'minimap')
      .attr('transform', 'translate(0,' + margin2.top + ')');

    this.minimap
      .append('defs')
      .append('svg:clipPath')
      .attr('id', 'clipMinimap')
      .append('svg:rect')
      .attr('width', this.width - 1)
      .attr('height', this.heightMinimap)
      .attr('x', 0)
      .attr('y', this.minimapTop);

    this.plotMinimap = this.minimap
      .append('g')
      .attr('clip-path', 'url(#clipMinimap)');

    var minimapLine = d3
      .line()
      .x((element: DateValuePair) => this.xMinimapScale(element.date))
      .y((element: DateValuePair) => yMinimapScale(element.field))
      .curve(d3.curveMonotoneX);

    this.plotMinimap
      .append('path')
      .datum(data)
      .attr('class', 'minimapLine')
      .attr('d', minimapLine)
      .attr('stroke', 'white')
      .attr('stroke-width', 1)
      .attr('fill', 'none')
      .attr('transform', 'translate(0,' + this.minimapTop + ')');

    this.minimap
      .append('g')
      .attr('class', 'axis axis--x')
      .attr(
        'transform',
        'translate(0,' + (this.heightMinimap + this.minimapTop) + ')'
      )
      .call(d3.axisBottom(this.xMinimapScale));

    if (this.fontScale != 1.0) {
      this.minimap.selectAll('text').attr('font-size', this.fontScale + 'em');
    }

    this.minimapBrush = d3
      .brushX()
      .extent([
        [0, 0],
        [this.width, this.heightMinimap],
      ])
      .on('brush end', () => {
        // only trigger change, if we are not manually updating the brush
        if (!this.updatingBrush) {
          this.minimapRangeChanged();
        }
      });

    this.minimap
      .append('g')
      .attr('class', 'brush')
      .call(this.minimapBrush)
      .call(this.minimapBrush.move, [
        this.width * this.minimapOffsetLeft,
        this.width * this.minimapOffsetRight,
      ])
      .attr('transform', 'translate(0,' + this.minimapTop + ')');

    this.onMinimap(
      this.plotMinimap,
      this.minimapTop,
      this.xMinimapScale,
      yMinimapScale
    );
  }

  private minimapRangeChanged(): void {
    let extent = d3.event.selection || this.xMinimapScale.range();
    this.setMinimapOffsets(
      extent[0] / this.width,
      extent[1] / this.width,
      false,
      true,
      true
    );
  }

  private addLegend(): void {
    var legend = this.svg
      .append('g')
      .attr('class', 'legend')
      .attr('fill', '#fff')
      .attr('width', this.width);

    var newX = 0;

    for (let tsc of this.timeseries) {
      if (tsc == undefined) {
        continue;
      }

      //add icon
      legend
        .append('rect')
        .attr('y', () => {
          return tsc.style == RenderStyle.line ? 4 * this.fontScale : 0;
        })
        .attr('x', newX)
        .attr('height', () => {
          return tsc.style == RenderStyle.line ? 3 : 10 * this.fontScale;
        })
        .attr('width', 40 * this.fontScale)
        .style('fill', tsc.color)
        .style('fill-opacity', () => {
          return tsc.style == RenderStyle.area ? this.AREA_FILL_OPACITY : 1.0;
        });

      //add description
      let textBox = legend
        .append('text')
        .attr('y', 9 * this.fontScale)
        .attr('x', newX + 50 * this.fontScale)
        .attr('font-size', this.fontScale + 'em')
        .text(tsc.label);

      newX +=
        (textBox.node().getBoundingClientRect().width + 110) * this.fontScale;
    }
    var legendWidth: number =
      legend.node().getBoundingClientRect().width * this.fontScale;
    legend.attr(
      'transform',
      'translate(' +
        (this.width / 2 - legendWidth / 2) +
        ',' +
        (this.height + 50) +
        ')'
    );
  }

  protected updateChart(chart: TimeseriesChart): void {
    // let extent = d3.event.selection || chart.xMinimapScale.range();
    let extent = [
      this.minimapOffsetLeft * this.width,
      this.minimapOffsetRight * this.width,
    ];
    chart.xScale.domain(
      extent.map(chart.xMinimapScale.invert, chart.xMinimapScale)
    );

    chart.xAxis.call(d3.axisBottom(chart.xScale));

    chart.plotCanvas
      .selectAll('rect')
      .attr('x', (element: DateValuePair) => chart.xScale(element.date));

    // adjust bar width to available space
    let numBars = Math.abs(
      (this.xScale.domain()[1].getTime() - this.xScale.domain()[0].getTime()) /
        this.minTimeDeltaMillis
    );

    let barWidth = Math.ceil((chart.width / numBars) * this.plotFactor);

    if (chart.barPlot) {
      chart.barPlot
        .join('rect')
        .attr('width', barWidth)
        .attr('transform', 'translate(-' + barWidth / 2 + ',0)');
    }

    chart.lineList.forEach((line) => {
      chart.plotCanvas.select(line.className).attr('d', line.update);
    });

    if (this.fontScale != 1.0) {
      this.xAxis.selectAll('text').attr('font-size', this.fontScale + 'em');
    }
  }

  protected idleDateRange(chart: TimeseriesChart): void {
    chart.idleTimeout = null;
    chart.build();
  }

  public dateRangeUpdate(start: Date, end: Date): void {
    this.startDate = start;
    this.endDate = end;

    if (!this.idleTimeout) {
      this.idleTimeout = setTimeout(() => this.idleDateRange(this), 500);
    }
  }

  protected sortAndFilterData(dataSet: DateValuePair[]): DateValuePair[] {
    let prepareList: DateValuePair[] = dataSet.sort((a, b) => {
      return a.date.getTime() - b.date.getTime();
    });

    return prepareList;
  }

  protected getSmallestTemporalDelta(data: DateValuePair[]): number {
    let minDelta = Number.MAX_SAFE_INTEGER;
    for (let i = 1; i < data.length; ++i) {
      let timeDeltaMillis = (data[i].date as any) - (data[i - 1].date as any);

      if (timeDeltaMillis > 0 && timeDeltaMillis < minDelta) {
        minDelta = timeDeltaMillis;
      }
    }

    return minDelta;
  }

  public toString(): string {
    return NAME + ' V' + VERSION.join('.');
  }
}
</script>