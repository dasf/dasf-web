import Layer from "ol/layer/Layer";

export enum CatalogItemStatus {
    none,
    unavailable,
    folder,
    loading,
    processing,
    finished,
    error
}

export type CatalogItemParameter = {
  name: string;
  type: string; // string, number, login
  value: string | number | [string, string];
  valuePresets?: string[];
}

export type CatalogItem = {
    id: number;
    name: string;
    status: CatalogItemStatus;
    description: string;
    disabledReason: string;
    disabled: boolean;
    needsRoi: boolean;
    parameters?: CatalogItemParameter[];
    call?: (addLayer: (layer: Layer) => void) => void;
    children?: CatalogItem[];
};

var itemCount = 0;

export function getCatalogItemId(): number {
    return ++itemCount;
}


export function getSelectedCatalogItems(ids: number[]): CatalogItem[] {
    let flattenList: CatalogItem[] = new Array();
    let catalog: CatalogItem[] = window['layer_catalog'];

    getFlattenList(catalog, flattenList);
    let selectedItems: CatalogItem[] = new Array();
    flattenList.forEach(element => {
        if (ids.includes(element.id)) {
            selectedItems.push(element);
        }
    })

    return selectedItems;
}

function getFlattenList(items: CatalogItem[], flattenList: CatalogItem[]): void {
    items.forEach(element => {
        flattenList.push(element);
        if (element.children) {
            getFlattenList(element.children, flattenList);
        }
    });
}
