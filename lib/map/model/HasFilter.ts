import Filter from './Filter';

export default interface IHasFilter {
    setFilter(filter: Filter): void;
    getFilter(): Filter | undefined;
}

export function isFilterable(obj: object): boolean {
    return obj && 'setFilter' in obj && 'getFilter' in obj;
}