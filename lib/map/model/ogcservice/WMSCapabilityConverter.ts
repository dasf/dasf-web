export interface WMSLayerData{
    name: string; 
    title: string; 
    layerInfo: any;
    type: string;
}

export class WMSCapabilityConverter{
    private capa: any;
    private layerData: WMSLayerData[] = new Array(); 

    constructor(capa: any | null){
        if(capa){
            this.capa = capa['WMS_Capabilities'];
        }
    }

    public getVersion(): string{
        var version: string|null = this.capa._attributes.version;
        return version;
    }

    public getProviderName(): string{
        var name: string = this.capa.Service.Name._text;
        if(!name){
            name = this.capa.Service.Name._cdata;
        }
        return name;
    }

    public getProviderTitle(): string{
        var title: string = this.capa.Service.Title._text;
        if(!title){
            title = this.capa.Service.Title._cdata;
        }
        if(!title){
            title = "";
        }
        return title;
    }

    public getProviderAbstract(): string{
        var abstract: string = this.capa.Service.Abstract._text;
        if(!abstract){
            abstract = this.capa.Service.Abstract._cdata;
        }
        if(!abstract){
            abstract = "";
        }
        return abstract;
    }

    public getLayers(): Array<WMSLayerData>{
        this.layerData = new Array();
        this.extractLayer(this.capa.Capability.Layer.Layer);
        return this.layerData;
    }

    public getLayerInfoLegenUrl(layerInfo: any): string{
        
        var legendURL: any;

        var url: string = "";
        if(layerInfo.Style){
            legendURL = layerInfo.Style.LegendURL
        }else if(layerInfo.LegendURL){
            
            legendURL = layerInfo.LegendURL;
        }

        if(legendURL){
            url = legendURL.OnlineResource._attributes["xlink:href"]
        }
        
        return url;
    }

    protected extractLayer(layers: any):void{
        for (var key in layers) {
            
            var layerInfo = layers[key];
            if (layerInfo.Name != undefined) {
                this.layerData.push({
                    name: this.getValue(layerInfo.Name),
                    title: this.getValue(layerInfo.Title),
                    layerInfo: layerInfo,
                    type: 'wms'
                  });
            }else if(layerInfo.Layer!=undefined){
                this.extractLayer(layerInfo.Layer);
            }
        }
    }

    protected getValue(nodeName: any):string{
        return (nodeName._text)? nodeName._text : nodeName._cdata;
    }

}