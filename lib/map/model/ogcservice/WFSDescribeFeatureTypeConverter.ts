import axios from 'axios';

export class WFSDescribeFeatureTypeConverter{

    public async loadDescribeFeatureDocument(url: string, categoryName:string){
        var url_path = url;
        let url_query = "";
        if(url.includes("?")){
          url_path = url.split("?")[0]
          url_query = url.split("?")[1]
        }

        if(url_query!="" && !url_query.endsWith('&')){
            url_query += '&';
        }
        if (!url_query.toLowerCase().includes('version')) {
            url_query += 'version=2.0.0';
        }
        if (!url_query.toLowerCase().includes('request')) {
            url_query += '&request=DescribeFeatureType';
        }
        if (!url_query.toLowerCase().includes('service')) {
            url_query += '&service=wfs';
        }
        url_query += "&typeName="+categoryName;

        let url_capa = url_path + "?" + url_query;

        var describeFeatureResult = await axios.get(url_capa);
        let describeFeatureData = describeFeatureResult.data;

        var oParse = new DOMParser();
        var doc: XMLDocument = oParse.parseFromString(describeFeatureData,'application/xml');

        return doc;
    }

    public getFeatureElements(doc:XMLDocument, categoryName:string): Array<[String, String]>{
        let featureElements: Array<[String, String]> = new Array();

        // add suffix of the complexType
        if(!categoryName.endsWith("Type")){
            categoryName += "Type";
        }

        // find category name
        var categories = doc.getElementsByTagName("xsd:element");
        for(var i=0;i< categories.length; i++){
            if(categories[i].getAttribute("type")==categoryName){
                categoryName = categories[i].getAttribute("name") as string;
                break;
            }
        }
        
        // add suffix name of the complexType
        if(!categoryName.endsWith("Type")){
            categoryName += "Type";
        }

        // 3 filter category by category name
        var categories = doc.getElementsByTagName("xsd:complexType");
        var category: Element | null = null ;
        for(var i=0;i< categories.length; i++){
            if(categories[i].getAttribute("name")==categoryName){
                category = categories[i];
                break;
            }
        }

        // iterate specific category
        if(category){
            var sequenceList: Element = category.getElementsByTagName("xsd:sequence")[0];
            var elements: HTMLCollection = sequenceList.children;
            for(let i=0; i< elements.length;i++){
                var name: string | null = elements[i].getAttribute("name");
                var type: string | null = elements[i].getAttribute("type");
                if(name && type){
                    featureElements.push([name, type])
                }
            }
        }        
        
        return featureElements;
    }
}