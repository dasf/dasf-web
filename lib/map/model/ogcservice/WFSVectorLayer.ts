import VectorLayer from 'ol/layer/Vector.js';
import { WFSDescribeFeatureTypeConverter } from './WFSDescribeFeatureTypeConverter';
import VectorSource from 'ol/source/Vector.js';
import GeoJSON from 'ol/format/GeoJSON';
import HasFilter from '../HasFilter';
import Filter from '../Filter';
import { URIUtil } from '../../../util/URIUtils';

export default class WFSVectorLayer extends VectorLayer implements HasFilter {

    private url: string = "";
    private layerTitle: string = "";
    private layerName: string = "";
    private filter: Filter | undefined;

    public constructor(options: object) {
        super(options);
        this.url = options['url'];
        this.layerTitle = super.getSource()["Title"];
        this.layerName = super.getSource()["LayerName"];

    }

    public async getFilterProperties() {
        let converter: WFSDescribeFeatureTypeConverter = new WFSDescribeFeatureTypeConverter();
        var doc: XMLDocument = await converter.loadDescribeFeatureDocument(this.url, this.layerName);
        var elements: Array<[String, String]> = converter.getFeatureElements(doc, this.layerName);

        return elements;
    }

    public getFilter(): Filter | undefined {
        return this.filter;
    }

    public setFilter(filter: Filter): void {
        this.filter = filter;

        // build query for wfs
        var queryObject: {} = {
            service: "WFS",
            version: "2.0.0",
            request: "GetFeature",
            typename: this.layerName,
            outputFormat: "application/json",
            srsname: "EPSG:3857",
            cql_filter: filter.getQuery()
        }

        var query = URIUtil.buildQuery(queryObject);
        var wfsURL = this.url + "?" + query;

        // console.log("WFSVectorLayer::query: " + query);

        // update source with filtered response
        var vectorSource = new VectorSource({
            format: new GeoJSON(),
            url: function (extent) {
                if (extent[0] != -Infinity) {
                    wfsURL += "&bbox=" + extent.join(',') + "";
                }

                return wfsURL;
            }
        });

        super.setSource(vectorSource);
    }

}