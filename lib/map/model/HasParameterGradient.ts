import Gradient, { HasGradient } from "../../colors/Gradient";
import { HasParameters } from "./Parameter";

export default interface HasParameterGradient extends HasGradient, HasParameters {
    setParameterAndGradient(name: string, gradient: Gradient);
}