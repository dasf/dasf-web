import DateRange from "../../dateSelection/daterangepicker/DateRange";

export default interface TemporalData {
    getDate(): Date;
    setDate(date: Date): void;
    getDateRange(): DateRange;
    getAllowedDates(): Date[];
}

export function isTemporalData(obj: object): boolean {
    return obj && 'getDate' in obj && 'setDate' in obj && 'getDateRange' in obj && 'getAllowedDates' in obj;
}
