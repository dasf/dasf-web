export default class NumericalParameter {
    private name: string;
    private min: number | undefined;
    private max: number | undefined;
    private fill: number | undefined;
    private dimensions: number[] | undefined;

    public constructor(name: string, min?: number, max?: number, fill?: number, dimensions?: number[]) {
        this.name = name;
        this.min = min;
        this.max = max;
        this.fill = fill;
        this.dimensions = dimensions;
    }

    public getName(): string {
        return this.name;
    }

    public getMin(): number | undefined {
        return this.min;
    }

    public getMax(): number | undefined {
        return this.max;
    }

    public setMin(min: number): void {
        this.min = min;
    }

    public setMax(max: number): void {
        this.max = max;
    }

    public getFill(): number | undefined {
        return this.fill;
    }

    public setFill(value: number): void {
        this.fill = value;
    }

    public hasMin(): boolean {
        return this.min !== undefined;
    }

    public hasMax(): boolean {
        return this.max !== undefined;
    }

    public hasFill(): boolean {
        return this.fill !== undefined;
    }

    public getDimensions(): number[]{
        return this.dimensions;
    }
}

export interface HasParameters {
    getAvailableParameters(): NumericalParameter[];
    setParameter(parameter: string): void;
}

const NoParam: NumericalParameter = new NumericalParameter('NoParam', 0, 0);

export { NoParam };