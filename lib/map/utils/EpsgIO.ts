import axios from 'axios';

interface EpsgIo {
    /**
     * Returns the proj4defs for the given epsg code
     * @param epsg
     */
    search(epsg: string): Promise<string>;
}

class EpsgIOImpl implements EpsgIo {

    public async search(epsg: string): Promise<string> {
        if (epsg.toUpperCase().startsWith('EPSG:')) {
            // remove prefix
            epsg = epsg.substring(5);
        }
        let result = await axios.get('https://epsg.io/?format=json&q=' + epsg);
        if (result.data && result.data['results'] && result.data['results'].length > 0) {
            let projectionResult = result.data['results'][0];
            return projectionResult['proj4'];
        }

        return null;
        // result.data['results']
        // return '+proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs';

        //     fetch('https://epsg.io/?format=json&q=' + query).then(function(response) {
        //   return response.json();
        // }).then(function(json) {
        //   var results = json['results'];
        //   if (results && results.length > 0) {
        //     for (var i = 0, ii = results.length; i < ii; i++) {
        //       var result = results[i];
        //       if (result) {
        //         var code = result['code'];
        //         var name = result['name'];
        //         var proj4def = result['proj4'];
        //         var bbox = result['bbox'];
        //         if (code && code.length > 0 && proj4def && proj4def.length > 0 &&
        //             bbox && bbox.length == 4) {
        //           setProjection(code, name, proj4def, bbox);
        //           return;
        //         }
        //       }
        //     }
        //   }
        //   setProjection(null, null, null, null);
        // });
    }
}

const EpsgIO: EpsgIo = new EpsgIOImpl();

export default EpsgIO;