import Vuetify, {
    VOverlay,
    VTabs,
    VTab,
    VTabItem,
    VTextField,
    VBtn,
    VSimpleTable,
    VIcon,
    VCard,
    VCardTitle,
    VCardActions,
    VCardText,
    VRow,
    VCol,
    VSpacer,
    VLayout,
    VFlex,
    VMenu,
    VDatePicker,
    VRangeSlider,
    VList,
    VListItem,
    VListGroup,
    VContainer,
    VListItemTitle,
    VListItemAction,
    VListItemContent,
    VCheckbox,
    VSlider,
    VProgressLinear,
    VProgressCircular,
    VStepper,
    VStepperStep,
    VStepperContent,
    VChipGroup,
    VChip,
    VAvatar,
    VDivider,
    VAutocomplete
} from 'vuetify/lib';
import { Vue } from 'vue-property-decorator';

import axios from 'axios';
import { CatalogItem } from './map/legend/DemapCatalogList';
import { DemapLayerAction, DemapLayerActionBase } from './map/legend/DemapLayerActions';

export class DEFrameworkInit {


    public init(): void {
        // console.log('[DEFrameworkInit] initializing de framework components');
        this.registerReverseProxy();
        this.loadVuetifyComponents();
    }

    private registerReverseProxy(): void {
        if (window['PROXY_URL']) {
            // console.log('[DEFrameworkInit] registering reverse proxy via request interceptor');

            // Add the CORS request interceptor to prepend the reverse proxy url
            axios.interceptors.request.use(function (config) {

                if (config && config.url && config.url.startsWith('http') && (config.proxy == undefined || config.proxy != false)) {
                    // absolute url - prepend proxy url to avoid CORS errors
                    config.url = window['PROXY_URL'] + config.url;
                    config.headers['X-Requested-With'] = 'XMLHttpRequest';
                }

                return config;
            }, function (error) {
                // Do something with request error
                return Promise.reject(error);
            });
        }
    }

    private loadVuetifyComponents(): void {
        // console.log('[DEFrameworkInit] registering vuetify framework components')
        Vue.use(Vuetify, {
            components: {
                VOverlay,
                VTabs,
                VTab,
                VTabItem,
                VTextField,
                VBtn,
                VSimpleTable,
                VIcon,
                VCard,
                VCardTitle,
                VCardActions,
                VCardText,
                VRow,
                VCol,
                VSpacer,
                VLayout,
                VFlex,
                VMenu,
                VDatePicker,
                VRangeSlider,
                VList,
                VListItem,
                VListGroup,
                VContainer,
                VListItemTitle,
                VListItemAction,
                VListItemContent,
                VCheckbox,
                VSlider,
                VProgressLinear,
                VProgressCircular,
                VStepper,
                VStepperStep,
                VStepperContent,
                VChipGroup,
                VChip,
                VAvatar,
                VDivider,
                VAutocomplete
            }
        });
    }

    public setCatalog(items: CatalogItem[]): void {
        // console.log('[DEFrameworkInit] registering layer catalog');
        window['layer_catalog'] = items;
    }

    public setDefaultRasterLayerActions(actions: DemapLayerActionBase[]) {
        // console.log('[DEFrameworkInit] registering default raster layer actions');
        window['default_raster_layer_actions'] = actions;
    }
}

const DEFramework: DEFrameworkInit = new DEFrameworkInit();

export default DEFramework;
