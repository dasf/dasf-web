export default class FileUtils {

    public static forceFileDownload(filename: string, data: any): void {
        let dataType: string = (data instanceof ArrayBuffer) ? "application/octet-stream" : "text/plain;charset=utf-8";
        const url = window.URL.createObjectURL(new Blob([data], { type: dataType }));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', filename);
        link.style.display = 'none';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
    
}