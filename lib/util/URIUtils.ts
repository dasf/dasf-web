export class URIUtil{

    /**
     * 
     * @param queryObject a key value list of a query
     * {p1: A, p2: B}
     * 
     * @return p1=A&p2=B
     */
    public static buildQuery(queryObject:{}):string{
        let query: string = "";

        for(let key in queryObject){
            if(queryObject[key]!=""){
                query += key+"="+queryObject[key]+"&";
            }
        }

        query = query.substr(0, query.length-1);

        return query;
    }
}