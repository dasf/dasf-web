export default class MathUtils {
    public static min(values: number[], ignore: number = Number.NaN): number {
        let min = Number.POSITIVE_INFINITY;

        for (let value of values) {
            if (!Number.isNaN(value) && value!=ignore && min > value) {
                min = value;
            }
        }

        if (min === Number.POSITIVE_INFINITY) {
            return Number.NaN;
        } else {
            return min;
        }
    }

    public static max(values: number[]): number {
        let max = Number.NEGATIVE_INFINITY;

        for (let value of values) {
            if (!Number.isNaN(value) && max < value) {
                max = value;
            }
        }

        if (max === Number.NEGATIVE_INFINITY) {
            return Number.NaN;
        } else {
            return max;
        }
    }

    public static replace(value: number, replacement: number, array: number[]): void {
        if (array && array.length > 0) {
            for (let i = 0; i < array.length; ++i) {
                if (array[i] === value) {
                    array[i] = replacement;
                }
            }
        }
    }

    public static isNumeric(num): boolean {
        return !isNaN(num);
    }
}