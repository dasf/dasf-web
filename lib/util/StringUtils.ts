export default class StringUtils {
    public static removeDoubleQuotes(s: string): string {
        if (StringUtils.hasDoubleQuotes(s)) {
            return s.substring(1, s.length - 1);
        } else {
            return s;
        }
    }

    public static removeAllDoubleQuotes(s: string[]): void {
        for(let i=0; i<s.length; ++i) {
            s[i] = this.removeDoubleQuotes(s[i]);
        }
    }

    public static hasDoubleQuotes(s: string): boolean {
        return s.charAt(0) === '"' && s.charAt(s.length - 1) === '"';
    }
}