import Histogram from "../model/Histogram";

export default class MathUtils {
    public static sort(values: number[] | string[]): number[] | string[] {
        return values.sort((a: number | string, b: number | string) => {
            if (a > b) {
                return 1;
            }

            if (a < b) {
                return -1;
            }

            return 0;
        })
    }

    public static clone<T>(array: T[]): T[] {
        return Object.assign([], array);
    }

    public static histogram(array: number[], numBuckets: number, noDataValue: number, minThreshold: number, maxThreshold: number): Histogram {
        if(numBuckets < 1) {
            throw new Error('invalid number of buckets for histogram generation: ' + numBuckets);
        }

        // init histogram array with number of buckets and fill with zeros
        let hist = new Array<number>(numBuckets);
        hist.fill(0);
        
        let stepRange = (maxThreshold-minThreshold) / numBuckets;
        
        for(let value of array) {
            if(Number.isNaN(value) || value == noDataValue || value < minThreshold || value > maxThreshold) {
                // skip value
                continue;
            }

            // inc bucket
            hist[Math.min(numBuckets-1, Math.floor((value-minThreshold)/stepRange))] += 1;
        }

        return new Histogram(hist, minThreshold, maxThreshold);
    }
}