import { DateSet } from "../../util/DateMap";

export class RefDateRangeModel {
    refDate: Date = new Date();
    allowedDates: DateSet = new DateSet();
    yearsInRange: Set<number> = new Set();
    backwardsDayRange: number = 0;
    forwardsDayRange: number = 0;

    constructor(private allowOutOfRangeYears: boolean = false) {}

    public addAllowedDates(dates: IterableIterator<Date>) {
      this.allowedDates.add(dates);

      if(this.allowOutOfRangeYears) {
        this.allowedDates.forEach((date: Date) => {
          this.yearsInRange.add(date.getFullYear());
        })
      }
  }

    public setAllowedDates(dates: IterableIterator<Date>) {
        this.allowedDates.clear();
        this.yearsInRange.clear();
        this.addAllowedDates(dates);
    }

    public isAllowedDate(date: number | string | Date): boolean {
        const dateDate = new Date(date);
        return !this.yearsInRange.has(dateDate.getFullYear()) || this.allowedDates.has(dateDate);
    }
}
