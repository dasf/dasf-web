export default class DateRange {

    minDate: Date = new Date(Date.now() - 157680000000); // 5 years ago
    maxDate: Date = new Date();
    private _selectedMinDate: Date = new Date(this.minDate.getTime() + 0.2 * (this.maxDate.getTime() - this.minDate.getTime()));
    private _selectedMaxDate: Date = new Date(this.minDate.getTime() + 0.8 * (this.maxDate.getTime() - this.minDate.getTime()));

    constructor(arg1?: DateRange | Date | string | number, arg2?: Date | string | number) {
        // handle min/max date inputs
        if (arg1 && arg2) {
            if (!(arg1 instanceof DateRange)) {
                this.minDate = new Date(arg1);
                this.maxDate = new Date(arg2);
                this._selectedMinDate = new Date(arg1);
                this.selectedMaxDate = new Date(arg2);
            }

            return;
        }

        // handle copy date range
        if (arg1 instanceof DateRange) {
            this.minDate = new Date(arg1.minDate);
            this.maxDate = new Date(arg1.maxDate);
            this._selectedMinDate = new Date(arg1._selectedMinDate);
            this._selectedMaxDate = new Date(arg1._selectedMaxDate);
        }
    }

    public get selectedMinDate(): Date {
        return this._selectedMinDate;
    }
    public set selectedMinDate(value: Date) {
        if (value == null) {
            return;
        }
        if (value < this.minDate) {
            this.minDate = value;
        }
        this._selectedMinDate = value;
    }

    public get selectedMaxDate(): Date {
        return this._selectedMaxDate;
    }
    public set selectedMaxDate(value: Date) {
        if (value == null) {
            return;
        }
        if (value > this.maxDate) {
            this.maxDate = value;
        }
        this._selectedMaxDate = value;
    }

}