export interface WorkflowSettingsObserver {
	settingsUpdate(type: string);
}

export abstract class WorkflowSettings {
	private _observers: WorkflowSettingsObserver[] = [];

	public addListener(observer: WorkflowSettingsObserver): void {
		if (!this._observers.includes(observer)) {
			this._observers.push(observer);
		}

		// console.log(this._observers);
	}

	public removeListener(observer: WorkflowSettingsObserver): void {
		// FIXME: why doesn't this work???
		const index = this._observers.indexOf(observer, 0);
		if (index > -1) {
			this._observers.splice(index, 1);
		}
	}

	public clearListeners() {
		this._observers = [];
	}

	protected callObservers(type: string): void {
		for (const observer of this._observers) {
			observer.settingsUpdate(type);
		}
	}
};

class UserSetting {
	private workflowSettingsMap: Map<string, WorkflowSettings> = new Map<string, WorkflowSettings>();

	public registerWorkflowSettings(workflowName: string, workflowSettings: WorkflowSettings) {
		// console.log("[UserSettings] registering workflowsettings for '" + workflowName + "'");
		this.workflowSettingsMap.set(workflowName, workflowSettings);
	}

	public getWorkflowSettings(workflowName: string): WorkflowSettings | undefined {
		return this.workflowSettingsMap.get(workflowName);
	}

	public hasWorkflowSettings(workflowName: string): boolean {
		return this.workflowSettingsMap.has(workflowName);
	}
}

export const userSetting = new UserSetting();
