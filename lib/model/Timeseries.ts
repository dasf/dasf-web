import { JsonProperty, JsonObject } from "json2typescript";
import { DateConverter, VersionConverter } from "./JsonSerializeHelper";

@JsonObject("DateValuePair")
export class DateValuePair {
    @JsonProperty("date", DateConverter)
    public date: Date = new Date();

    @JsonProperty("field", Number)
    public field: Number = -1;

    public constructor(date?: Date, field?: Number) {
        this.date = date || new Date();
        this.field = field || 0;
    }
}
@JsonObject("Header")
export class Header {
    @JsonProperty("name", String)
    public name: string;
    @JsonProperty("unit", String)
    public unit: string;

    public constructor(name?: string, unit?: string) {
        this.name = name || "";
        this.unit = unit || "";
    }
}

@JsonObject("Timeseries")
export class Timeseries {

    @JsonProperty("gid", Number, true)
    public gid: number = 0;
    @JsonProperty("data", [DateValuePair])
    public data: DateValuePair[] = [];
    @JsonProperty("header", Header)
    public header: Header = new Header();
    @JsonProperty("version", VersionConverter)
    public version: string = "0";

    /**
     * Returns the minimum and maximum value of this time-series
     */
    public getValueBounds(): [number, number] {
        let min = Number.MAX_VALUE;
        let max = -Number.MAX_VALUE;

        for (let pair of this.data) {
            min = Math.min(pair.field.valueOf(), min);
            max = Math.max(pair.field.valueOf(), max);
        }

        return [min, max];
    }

    /**
     * Return the minimum and maximum date of the time-series
     */
    public getDateBounds(): [Date, Date] {
        let min = this.data[0].date;
        let max = min;

        for (let pair of this.data) {
            if (min > pair.date) {
                min = pair.date;
            }
            if (max < pair.date) {
                max = pair.date;
            }
        }

        return [min, max];
    }

    public getSubsection(startDate: Date, endDate: Date): DateValuePair[] {
        let slice: DateValuePair[] = [];

        for (let e of this.data) {
            if (e.date >= startDate && e.date <= endDate) {
                slice.push(e);
            }
        }

        return slice;
    }

}
