import MathUtils from "../util/MathUtils";

export default class Histogram {
    public static readonly empty: Histogram = new Histogram([], 0, 0);

    public readonly bucketSize: number;

    constructor(public readonly values: number[], public readonly min: number, public readonly max: number) {
        this.bucketSize = (this.max - this.min) / this.values.length;
    }

    public getX(idx: number): number {
        return this.min + this.bucketSize * idx;
    }

    public getMaxValue(): number {
        return MathUtils.max(this.values);
    }
}