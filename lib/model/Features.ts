export interface Geometry {
    coordinates: number[];
    type: string;
}

export interface Feature {
    geometry: Geometry;
    type: string;
}

export interface FeatureCollection<T extends Feature> {
    type: string;
    features: T[];
}

