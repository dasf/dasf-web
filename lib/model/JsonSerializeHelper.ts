import { JsonCustomConvert, JsonConverter } from "json2typescript";

@JsonConverter
export class DateConverter implements JsonCustomConvert<Date>{

    serialize(date: Date): any {
        return date.toJSON();
    }
    deserialize(date: any): Date {
        let stringDate: string = date as string;

        // the date parser assumes different timezones depending on the date format:
        // Date.parse('2013-06-09') => 1370736000000
        // Date.parse('2013-06-09T00:00:00') => 1370728800000
        // Date.parse('2013-06-09T00:00:00Z') => 1370736000000

        if (stringDate.match("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}$")) {
            // add missing 'Z' to timestamp
            stringDate += 'Z';
        }
        return new Date(stringDate);
    }

}

@JsonConverter
export class VersionConverter implements JsonCustomConvert<String | Number>{

    serialize(value: string | number): any {
        return value.toString();
    }

    deserialize(value: string | number): string {
        return value.toString();
    }

}
