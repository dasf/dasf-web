import PulsarConnection from "./PulsarConnection";
import { PulsarModuleResponse, PulsarProgressReport, PulsarModuleRequest, PulsarModuleRequestReceipt } from "./PulsarMessages";
import { DigitalEarthUrlBuilder } from "./PulsarUrlBuilder";

/**
 * Works with
 * https://git.geomar.de/digital-earth/dasf/dasf-messaging-python/-/blob/master/ExampleMessageConsumer.py
 */
const PULSAR_TOPIC = "mytesttopic";

export default class ExampleModuleMessaging {
    private pulsarConnection: PulsarConnection;

    public constructor() {
        this.pulsarConnection = new PulsarConnection(new DigitalEarthUrlBuilder(PULSAR_TOPIC));
    }

    public isConnected(): boolean {
        return this.pulsarConnection.isBackendConnected();
    }

    public sendHelloWorldRequest(greetMessage: string, repeatMessage: string, repeat: number): Promise<Record<string, unknown>> {
      return new Promise((resolve: (value: Record<string, unknown>) => void, reject: (reason: string) => void) => {
        this.pulsarConnection.sendRequest(this.createHelloWorldRequest(greetMessage, repeatMessage, repeat), (response: PulsarModuleResponse) => {
            if(response.properties.status == 'success') {
              resolve(JSON.parse(atob(response.payload)))
            } else {
              reject(atob(response.payload))
            }
        }, 
        null, 
        (receipt: PulsarModuleRequestReceipt) => {
          reject(receipt.errorMsg)  
        });
      })
    }

    private createHelloWorldRequest(greetMessage: string, repeatMessage: string, repeat: number): PulsarModuleRequest {
        let request = PulsarModuleRequest.createRequestMessage();

        let moduleCall: object = {
          func_name: "hello_world",
          message: greetMessage,
          repeat: repeat,
          greet_message: repeatMessage,
        }

        request.payload = btoa( JSON.stringify( moduleCall ) )

        return request;
    }

    public sendProgressReportRequest(onProgress: (report: PulsarProgressReport) => void): Promise<void> {
      return new Promise((resolve: () => void, reject: (reason: string) => void) => {
        this.pulsarConnection.sendRequest(this.createProgressDemoRequest(), (response: PulsarModuleResponse) => {
            if(response.properties.status == 'success') {
              resolve()
            } else {
              reject(atob(response.payload))
            }
        }, 
        onProgress, 
        (receipt: PulsarModuleRequestReceipt) => {
          reject(receipt.errorMsg)  
        });
      })
    }

    private createProgressDemoRequest(): PulsarModuleRequest {
      let request = PulsarModuleRequest.createRequestMessage();

      let moduleCall: object = {
        func_name: "report_progress_demo",
        reporter: { report_type: 'tree', report_id: 'root', status: 'running', step_message: 'some root progress message', steps: 0, children: [] },
      }

      request.payload = btoa( JSON.stringify( moduleCall ) )

      return request;
  }
}
