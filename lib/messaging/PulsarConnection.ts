import { PulsarModuleResponse, PulsarProgressReport, PulsarAcknowledgment, PulsarModuleRequest, PulsarModuleRequestReceipt, PropertyKeys, MessageType } from "./PulsarMessages";
import { PulsarUrlBuilder } from "./PulsarUrlBuilder";
import { JsonConvert } from "json2typescript";

export default class PulsarConnection {

    private outgoingChannel!: WebSocket;
    private incomingChannel!: WebSocket;

    private pulsarProducerURL: string;
    private pulsarConsumerURL: string;

    private topic: string;
    private consumeTopic: string;

    private jsonCoder: JsonConvert;

    private contextCounter: number = 0;

    private responseCallbacks: Map<string, Function> = new Map();
    private progressCallbacks: Map<string, Function> = new Map();
    private errorCallbacks: Map<string, Function> = new Map();
    private finishedContexts: Set<string> = new Set();
    private contextResponseFragments: Map<string, PulsarModuleResponse[]> = new Map();

    private pendingRequests: PulsarModuleRequest[] = [];

    private waitingForPong: boolean = false;
    private connectionTimeout = 10000; // 10sec

    private backendConsumerConnected = false;

    private connectionError = '';
    private onConnectionError: (errorMsg: string) => void = undefined;

    constructor(urlBuilder: PulsarUrlBuilder, onConnectionError?: (errorMsg: string) => void) {
        //tenant/namespace/topic/subscription
        //public/default/producerTopic/registry/
        this.onConnectionError = onConnectionError;
        this.topic = urlBuilder.topic;
        this.consumeTopic = urlBuilder.consumeTopic

        this.pulsarProducerURL = urlBuilder.pulsarProducerURL
        this.pulsarConsumerURL = urlBuilder.pulsarConsumerURL

        this.jsonCoder = new JsonConvert();

        this.assureConnected();
    }

    private connect() {
        this.outgoingChannel = new WebSocket(this.pulsarProducerURL);
        this.incomingChannel = new WebSocket(this.pulsarConsumerURL);
        this.registerListener();
        this.getInitialModuleInfo();

        setInterval(() => this.checkConnection(), this.connectionTimeout);

    }

    private isConnected(): boolean {
        return this.outgoingChannel && this.incomingChannel && this.outgoingChannel.readyState == WebSocket.OPEN && this.incomingChannel.readyState == WebSocket.OPEN;
    }

    private isConnecting(): boolean {
        return this.outgoingChannel && this.incomingChannel && this.outgoingChannel.readyState != WebSocket.OPEN && this.incomingChannel.readyState != WebSocket.OPEN;
    }

    private assureConnected(): boolean {
        if (this.isConnected()) {
            return true;
        } else {
            if (!this.isConnecting()) {
                this.connect();
            }

            return false;
        }
    }

    public hasPendingRequests(): boolean {
        return this.pendingRequests.length > 0;
    }

    private sendPendingRequests(): void {
        if (this.hasPendingRequests()) {
            // console.log('sending pending request (' + this.pendingRequests.length + ') ...');

            let request = this.pendingRequests.shift();
            this.outgoingChannel.send(JSON.stringify(request));

            setTimeout(() => this.sendPendingRequests(), 200);
        }
    }

    protected registerListener(): void {
        const onError = (ev: Event) => {
          if(!this.hasConnectionError() && this.isConnecting()) {
            this.connectionError = 'Connection to ' + (ev.target as WebSocket).url + ' failed.'
            if(this.onConnectionError) {
              this.onConnectionError(this.connectionError);
            }
          }
        };

        this.outgoingChannel.addEventListener('error', onError);
        this.incomingChannel.addEventListener('error', onError);

        this.outgoingChannel.addEventListener('open', (ev: Event) => {
            // console.log("pulsar outgoing channel established");
            // connection established - reset past connection errors
            this.connectionError = '';

            // listen for acknowledgments of outgoing messages
            this.outgoingChannel.addEventListener('message', (ev: Event) => {
                // console.log("[pulsar outgoing] request acknowledged");
                let receipt: PulsarModuleRequestReceipt = this.jsonCoder.deserializeObject(JSON.parse(ev['data']), PulsarModuleRequestReceipt);
                if (receipt.isOk()) {
                    // the request was acknowledged - remove error callback
                    this.errorCallbacks.delete(receipt.context);
                } else {
                    // the request returned an error
                    console.error(receipt);

                    // remove the registered callback - since there won't be a response
                    this.responseCallbacks.delete(receipt.context);

                    // trigger error callback
                    let onErrorCallback = this.errorCallbacks.get(receipt.context);
                    if (onErrorCallback) {
                        // remove callback from map
                        this.errorCallbacks.delete(receipt.context);

                        // trigger error callback
                        onErrorCallback(receipt);
                    } else {
                        console.error("received error receipt for an unknwon context: " + receipt.context);
                    }
                }
            });
        });

        this.incomingChannel.addEventListener('open', (ev: Event) => {
            // connection established - reset past connection errors
            this.connectionError = '';

            // listen for incoming messages
            this.incomingChannel.addEventListener('message', (ev: Event) => {
                // console.log("incoming message...");
                try {
                    // parse and decode message
                    let data = JSON.parse(ev['data'])
                    let response: PulsarModuleResponse = this.jsonCoder.deserializeObject(data, PulsarModuleResponse);

                    // acknowledge the message
                    this.acknowledgeMessage(response);

                    // extract request context and trigger callback
                    let requestContext = response.getRequestContext();
                    if (this.finishedContexts.has(requestContext)) {
                        // this request has already been processed - ignore duplicate
                        return;
                    }

                    if (response.getMessageType() == MessageType.Progress) {
                        let progressReport: PulsarProgressReport = this.jsonCoder.deserializeObject(
                            JSON.parse(atob(data.payload)),
                            PulsarProgressReport
                        );

                        let progressCallback = this.progressCallbacks.get(requestContext);
                        if (progressCallback) {
                            progressCallback(progressReport, response.properties);
                        }
                    } else {
                        // is this just a fragment - or the whole thing ?
                        if ( response.isFragmented() ) {
                            // fragmented message
                            if(this.contextResponseFragments.has(requestContext)) {
                                // we already have some fragments - append
                                let fragments: PulsarModuleResponse[] = this.contextResponseFragments.get(requestContext);
                                fragments.push(response);

                                // console.log('fragment ' + response.getFragmentId() + ' of ' + response.getNumberOfFragments() + ' received');

                                if(fragments.length == response.getNumberOfFragments()) {
                                    // console.log('assembling fragmented payload');
                                    // all fragments received - assemble
                                    fragments = fragments.sort((a: PulsarModuleResponse, b: PulsarModuleResponse) => a.getFragmentId() - b.getFragmentId())
                                    let data: string = '';
                                    for(let frag of fragments) {
                                        data += frag.payload;
                                    }

                                    // pretend the last response message contained the entire data
                                    response.payload = data;

                                    // clear the fragment data
                                    this.contextResponseFragments.delete(requestContext);
                                } else {
                                    // fragments left - continue
                                    return;
                                }
                            } else {
                                // first fragmented message received - store
                                this.contextResponseFragments.set(requestContext, [response]);
                                return;
                            }

                        }

                        // get callback function for context
                        let responseCallback = this.responseCallbacks.get(requestContext);
                        if (responseCallback) {
                            // remove it from the map
                            this.responseCallbacks.delete(requestContext);
                            this.progressCallbacks.delete(requestContext);
                            this.errorCallbacks.delete(requestContext);

                            this.finishedContexts.add(requestContext);

                            // trigger callback
                            responseCallback(response);
                        } else {
                            console.warn("received a response for an unknown context: " + requestContext, response);
                        }
                    }



                } catch (e) {
                    console.error(e);
                }

            });
        });
    }

    private checkConnection(): void {
        if (this.waitingForPong) {
            // we sent a ping but still waiting for the pong - timeout
            this.backendConsumerConnected = false;
            console.warn("Ping to backend module timed out.");
        }

        // send ping
        //console.log('sending ping');
        this.waitingForPong = true;

        // create the ping message
        let ping = PulsarModuleRequest.createPingMessage();

        // send the request
        this.sendRequest(
            ping,
            pong => {
                // we received a pong
                this.waitingForPong = false;
                this.backendConsumerConnected = true;

                // send pending request if any
                this.sendPendingRequests();
            },
            progress => {

            },
            errorReceipt => {
                console.warn(errorReceipt.errorMsg);
            });
    }


    public sendRequest(requestMessage: PulsarModuleRequest, onResponse: (responseMsg: PulsarModuleResponse) => void, onProgress?: (responseMsg: PulsarProgressReport, msgProps?: object) => void, onError?: (receipt: PulsarModuleRequestReceipt) => void) {
        // create context id
        requestMessage.context = String(++this.contextCounter);

        // set response topic prop
        requestMessage.properties[PropertyKeys.ResponseTopic] = this.consumeTopic;
        requestMessage.properties[PropertyKeys.RequestContext] = requestMessage.context;

        // store context->callback mapping
        this.responseCallbacks.set(requestMessage.context, onResponse);
        this.progressCallbacks.set(requestMessage.context, onProgress);
        this.errorCallbacks.set(requestMessage.context, onError);

        if ((requestMessage.getMessageType() == MessageType.Request || requestMessage.getMessageType() == MessageType.Info) && !this.backendConsumerConnected) {
            // store as pending request
            // console.log('storing request due to unconnected backend module')
            if(this.hasConnectionError() && onError != undefined) {
              const receipt = new PulsarModuleRequestReceipt();
              receipt.context = requestMessage.context;
              receipt.errorMsg = this.connectionError;

              this.responseCallbacks.delete(requestMessage.context);
              this.progressCallbacks.delete(requestMessage.context);
              this.errorCallbacks.delete(requestMessage.context);

              onError(receipt);
            }

            this.pendingRequests.push(requestMessage);
            return;
        }

        if (this.assureConnected()) {
            // console.log("sending request...");

            // send the message as a json string
            let msg_str = JSON.stringify(requestMessage);
            this.outgoingChannel.send(msg_str);
        }
    }

    private acknowledgeMessage(message: PulsarModuleResponse) {
        let ack = this.jsonCoder.serializeObject(new PulsarAcknowledgment(message.messageId));
        this.incomingChannel.send(JSON.stringify(ack));
    }

    public close() {
        this.outgoingChannel.close();
        this.incomingChannel.close();
    }

    private getInitialModuleInfo(): void {
        if (this.backendConsumerConnected) {
            // we are connected - request module info
            this.getModuleInfo((info: object) => {
                console.info('module for topic ' + this.topic + ' provided the following module info: ', info);
            });
        } else {
            // not (yet) connected request module info later
            setTimeout(() => this.getInitialModuleInfo(), 1000);
        }
    }

    public getModuleInfo(onModuleInfo: (info: object) => void): void {
        // console.log('sending module info request');
        this.sendRequest(PulsarModuleRequest.createInfoMessage(), (res: PulsarModuleResponse) => {
            try {
                onModuleInfo(JSON.parse(res.properties['info']));
            } catch (e) {
                // unable to parse
                console.warn('Unable to retrieve backend module capabilities, got: ' + res.properties['info']);
            }
        }, (res: PulsarProgressReport) => {
            try {
                console.info('getModuleInfo success');
            } catch (e) {
                console.warn('getModuleInfo failure');
            }
        }, (receipt: PulsarModuleRequestReceipt) => {
            throw new Error(receipt.errorMsg);
        })
    }

    public isBackendConnected(): boolean {
        return this.backendConsumerConnected;
    }

    public hasConnectionError(): boolean {
      return this.connectionError.length > 0;
    }

    public getConnectionErrorMessage(): string {
      return this.connectionError;
    }

}
