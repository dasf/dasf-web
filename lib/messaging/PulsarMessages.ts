import { JsonObject, JsonProperty, Any } from "json2typescript";

export enum MessageType {
    Ping = 'ping',
    Pong = 'pong',
    Info = 'info',
    Request = 'request',
    Response = 'response',
    Log = 'log',
    Progress = 'progress'
}

export enum PropertyKeys {
    ResponseTopic = "response_topic",
    RequestContext = "requestContext",
    RequestMessageId = "requestMessageId",
    MessageType = "messageType"
}

export enum Status {
    Success = "success",
    Error = "error",
    Running = "running"
}

@JsonObject("PulsarModuleRequest")
export class PulsarModuleRequest {

    @JsonProperty("payload", String)
    payload: string = "";

    context: string = "";

    @JsonProperty("properties", Any)
    properties: any;

    private static createMessage(type: MessageType): PulsarModuleRequest {
        let msg = new PulsarModuleRequest();
        msg.properties = {}
        msg.properties[PropertyKeys.MessageType] = type;
        return msg;
    }

    public static createRequestMessage(): PulsarModuleRequest {
        return PulsarModuleRequest.createMessage(MessageType.Request)
    }

    public static createPingMessage(): PulsarModuleRequest {
        return PulsarModuleRequest.createMessage(MessageType.Ping)
    }

    public static createPongMessage(): PulsarModuleRequest {
        return PulsarModuleRequest.createMessage(MessageType.Pong)
    }

    public static createInfoMessage(): PulsarModuleRequest {
        return PulsarModuleRequest.createMessage(MessageType.Info);
    }

    public getMessageType() {
        return this.properties[PropertyKeys.MessageType];
    }

}

@JsonObject("PulsarModuleRequestReceipt")
export class PulsarModuleRequestReceipt {

    @JsonProperty("result", String)
    result: string = "";

    @JsonProperty("messageId", String, true)
    messageId: string = "";

    @JsonProperty("errorMsg", String, true)
    errorMsg: string = "";

    @JsonProperty("context", String, true)
    context: string = "";

    public isOk(): boolean {
        return this.result == "ok";
    }
}

@JsonObject("PulsarAcknowledgment")
export class PulsarAcknowledgment {

    @JsonProperty("messageId", String)
    messageId: string = "";

    constructor(messageId?: string) {
        if (messageId) {
            this.messageId = messageId;
        }
    }
}

@JsonObject("PulsarModuleResponse")
export class PulsarModuleResponse {

    @JsonProperty("messageId", String)
    messageId: string = "";

    @JsonProperty("payload", String)
    payload: string = "";

    @JsonProperty("publishTime", String)
    publishTime: string = "";

    @JsonProperty("properties")
    properties: any = undefined;

    public getMessageType(): string {
        if (this.properties) {
            let msgType = this.properties[PropertyKeys.MessageType];
            if (msgType && msgType.trim().length > 0) {
                return msgType;
            }
        }

        throw new Error("Missing or empty message type property")
    }

    public getRequestContext(): string {
        if (this.properties) {
            let context = this.properties[PropertyKeys.RequestContext];
            if (context) {
                return context;
            }
        }

        throw new Error("Missing or empty request context property");
    }

    public getRequestMessageId(): string {
        if (this.properties) {
            let messageId = this.properties[PropertyKeys.RequestMessageId];
            if (messageId) {
                return messageId;
            }

        }

        throw new Error("Missing or empty request messageId property");
    }

    public isFragmented(): boolean {
        return this.properties != undefined && this.properties.hasOwnProperty('num_fragments') && this.properties.hasOwnProperty('fragment');
    }

    public getFragmentId(): number {
        if (this.isFragmented()) {
            return this.properties['fragment'];
        }

        throw new Error('Unable to request fragment id from unfragmented response.');
    }

    public getNumberOfFragments(): number {
        if (this.isFragmented()) {
            return this.properties['num_fragments'];
        }

        throw new Error('Unable to request number of fragments from unfragmented response.');
    }

}

@JsonObject("PulsarProgressReport")
export class PulsarProgressReport {

    @JsonProperty("report_type", String)
    report_type: string = ""

    @JsonProperty("report_id", String)
    report_id: string = "";

    @JsonProperty("step_message", String)
    step_message: string = "";

    @JsonProperty("steps", Number)
    steps: number = 0;

    @JsonProperty("status", String)
    status: string = '';

    @JsonProperty("children", [PulsarProgressReport])
    children: PulsarProgressReport[] = undefined;

    public hasError(): boolean {
      return this.status == Status.Error
    }

    public hasSuccess(): boolean {
      return this.status == Status.Success
    }

    public isRunning(): boolean {
      return this.status == Status.Running
    }

    public isComplete(): boolean {
      return !this.isRunning()
    }
}
