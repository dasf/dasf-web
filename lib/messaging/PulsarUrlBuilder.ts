/**
 * Interface providing pulsar connection urls and parameters
 */
export interface PulsarUrlBuilder {
    /**
     * Which topic is used?
     */
    topic: string
    /**
     * The topic on that the consumer listens.
     * Needed to get the results for the backend module.
     */
    consumeTopic: string

    /**
     * A url that allows to create a consumer with the pulsar
     * web socket api:
     * 
     * A url can look similar to this:
     * ws://%host%:%port%/ws/v2/%type%/non-persistent/public/digital-earth/%topic%/%subscription%
     * 
     * with type = 'consumer'
     * and topic = consumeTopic
     * 
     */
    pulsarConsumerURL: string

    /** See pulsarConsumerURL for an explanation. */
    pulsarProducerURL: string
}

enum Type {
  producer = 'producer',
  consumer = 'consumer',
}

/**
 * Default url builder implementation based on host:port, namespace and topic
 */
export class DefaultPulsarUrlBuilder implements PulsarUrlBuilder {
  private static readonly BASE_PULSAR_URL: string = "ws://%host%:%port%/ws/v2/%type%/non-persistent/public/%namespace%/%topic%/";

  readonly topic: string;
  readonly consumeTopic: string;
  readonly pulsarConsumerURL: string;
  readonly pulsarProducerURL: string;


  constructor(host: string, port: string, namespace: string, topic: string) {
    this.topic = topic
    this.consumeTopic = this.generateConsumeTopic(topic);
    this.pulsarConsumerURL = this.buildUrl(host, port, namespace, Type.consumer, this.consumeTopic)
    this.pulsarProducerURL = this.buildUrl(host, port, namespace, Type.producer, this.topic)
  }

  protected buildUrl(host: string, port: string, namespace: string, type: Type, topic: string): string {
    let url = DefaultPulsarUrlBuilder.BASE_PULSAR_URL
      .replace("%host%", host)
      .replace("%port%", port)
      .replace("%namespace%", namespace)
      .replace("%type%", type)
      .replace("%topic%", topic);

    if (type == Type.consumer) {
      // append subscription
      url += this.generateSubscriptionPrefix() + "?subscriptionType=Exclusive";
    }

    return url;
  }

  protected generateConsumeTopic(topic: string): string {
    return topic + "_" + this.generateRequestToken()
  }

  protected generateSubscriptionPrefix(): string {
    return "web-frontend-" + new Date().toISOString();
  }

  protected generateRequestToken(): string {
    return Math.random().toString(16).slice(2);
  }
}

export class DigitalEarthUrlBuilder extends DefaultPulsarUrlBuilder {

  private static readonly HOST = "rz-vm154.gfz-potsdam.de";
  private static readonly PORT = "8082";
  private static readonly NAMESPACE = "digital-earth"

  constructor(topic: string) {
    super(DigitalEarthUrlBuilder.HOST, DigitalEarthUrlBuilder.PORT, DigitalEarthUrlBuilder.NAMESPACE, topic);
  }
}