export default interface CatalogCrawler {
	startCrawling(basePath: string, recursive: boolean, datasetSuffix: string, onDatasetDiscovered: (datasetPath: string) => void, onFinish: () => void): void;
}